from unittest import main, TestCase
from Diplomacy import diplomacy_solve, process_input

TEST_CASES = (
    (
'''A Madrid Hold'''.split('\n')
,
'''A Madrid'''
    ),

    (
'''A Madrid Hold
B Barcelona Move Madrid
C London Support B'''.split('\n')
,
'''A [dead]
B Madrid
C London'''
    ),

    (
'''A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Move London'''.split('\n')
,
'''A [dead]
B [dead]
C [dead]
D [dead]'''
    ),

(
'''A Madrid Hold
B Barcelona Move Madrid
C London Move Barcelona'''.split('\n')
,
'''A [dead]
B [dead]
C Barcelona'''
    )
)


class TestDiplomacy(TestCase):
    def test_solve_0(self):
        inp, out = TEST_CASES[0]
        inp = process_input(inp)
        self.assertEqual(diplomacy_solve(inp), out)

    def test_solve_1(self):
        inp, out = TEST_CASES[1]
        inp = process_input(inp)
        self.assertEqual(diplomacy_solve(inp), out)

    def test_solve_2(self):
        inp, out = TEST_CASES[2]
        inp = process_input(inp)
        self.assertEqual(diplomacy_solve(inp), out)

    def test_solve_3(self):
        inp, out = TEST_CASES[3]
        inp = process_input(inp)
        self.assertEqual(diplomacy_solve(inp), out)


if __name__ == "__main__":  # pragma: no cover
    main()
